package com.example.itunesdemo.core.data

import com.example.itunesdemo.core.data.dao.BaseDao
import com.example.itunesdemo.core.data.network.Api
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class BaseDaoTests {

    @MockK
    lateinit var api: Api

    lateinit var dao: BaseDao

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setErrorHandler {}
        dao = spyk(BaseDao(api))
    }

    @Test
    fun testSuccessExecution() {
        val mockService = Single.just(1)

        val mockedCallback: (Int) -> Unit = mockk()
        val callback = slot<(Int) -> Unit>()

        val mockedErrorCallback: (Throwable) -> Unit = mockk()
        val errorCallback = slot<(Throwable) -> Unit>()

        dao.executeServiceCall(mockService, mockedCallback, mockedErrorCallback)
        verify { dao.executeServiceCall(mockService, capture(callback), capture(errorCallback)) }
        assertTrue(callback.isCaptured)
        assertTrue(errorCallback.isCaptured)
        assertNotNull(callback.captured)
        assertNotNull(errorCallback.captured)
    }

}