package com.example.itunesdemo.detail.viewmodel

import com.example.itunesdemo.core.data.network.Api
import com.example.itunesdemo.core.data.network.model.SearchResponse
import com.example.itunesdemo.core.data.network.model.Song
import com.example.itunesdemo.detail.data.repository.DetailRepository
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeoutException

class DetailViewModelTests {

    @MockK
    lateinit var api: Api

    @MockK
    lateinit var mockedResult: SearchResponse

    @MockK
    lateinit var detailRepository: DetailRepository

    private lateinit var viewModel: DetailViewModel

    private lateinit var mockedSongList: List<Song>

    private val someTerm = "some term"

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        createMockedList()
    }

    @Test
    fun testSuccessSearch() {
        setupSuccessApi()
        viewModel.currentSong = getSampleSong(someTerm)
        testCallbacks()
    }

    @Test
    fun testBadSearch() {
        setupBadApi()
        viewModel.currentSong = getSampleSong(someTerm)
        testCallbacks()
    }

    private fun testCallbacks() {
        val callback = slot<(List<Song>) -> Unit>()
        val errorCallback = slot<(String) -> Unit>()

        verify {
            detailRepository.searchByAlbum(
                someTerm,
                capture(callback),
                capture(errorCallback)
            )
        }
        Assert.assertTrue(callback.isCaptured)
        Assert.assertTrue(errorCallback.isCaptured)
        Assert.assertNotNull(callback.captured)
        Assert.assertNotNull(errorCallback.captured)
    }

    private fun setupSuccessApi() {
        every { mockedResult.results } returns mockedSongList
        every { api.searchSong(any()) } returns Single.just(mockedResult)
        viewModel = spyk(DetailViewModel(detailRepository))
    }

    private fun setupBadApi() {
        every { mockedResult.results } returns mockedSongList
        every { api.searchSong(any()) } returns Single.error(TimeoutException())
        viewModel = spyk(DetailViewModel(detailRepository))
    }

    private fun createMockedList() {
        val songA = getSampleSong("A")
        val songB = getSampleSong("B")
        mockedSongList = listOf(songA, songB)
    }

    private fun getSampleSong(someString: String) =
        Song(
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString
        )
}