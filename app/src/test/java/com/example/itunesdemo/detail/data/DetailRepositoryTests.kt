package com.example.itunesdemo.detail.data

import com.example.itunesdemo.core.data.network.Api
import com.example.itunesdemo.core.data.network.model.SearchResponse
import com.example.itunesdemo.core.data.network.model.Song
import com.example.itunesdemo.detail.data.repository.DetailRepository
import com.example.itunesdemo.list.data.dao.SearchTermDao
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeoutException

class DetailRepositoryTests {

    @MockK
    lateinit var api: Api

    @MockK
    lateinit var searchTermDao: SearchTermDao

    @MockK
    lateinit var mockedResult: SearchResponse

    lateinit var detailRepository: DetailRepository

    lateinit var mockedSongList: List<Song>

    private val someTerm = "some term"

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setErrorHandler {}
        createMockedList()
    }

    @Test
    fun testSuccessSearch() {
        setupSuccessApi()
        testCallbacks()
    }

    @Test
    fun testBadSearch() {
        setupBadApi()
        testCallbacks()
    }

    private fun testCallbacks() {
        val mockedCallback: (List<Song>) -> Unit = mockk()
        val callback = slot<(List<Song>) -> Unit>()

        val mockedErrorCallback: (String) -> Unit = mockk()
        val errorCallback = slot<(String) -> Unit>()

        detailRepository.searchByAlbum(someTerm, mockedCallback, mockedErrorCallback)

        verify {
            detailRepository.searchByAlbum(
                someTerm,
                capture(callback),
                capture(errorCallback)
            )
        }
        Assert.assertTrue(callback.isCaptured)
        Assert.assertTrue(errorCallback.isCaptured)
        Assert.assertNotNull(callback.captured)
        Assert.assertNotNull(errorCallback.captured)
    }

    private fun setupSuccessApi() {
        every { mockedResult.results } returns mockedSongList
        every { api.searchSong(any()) } returns Single.just(mockedResult)
        detailRepository = spyk(DetailRepository(searchTermDao))
    }

    private fun setupBadApi() {
        every { mockedResult.results } returns mockedSongList
        every { api.searchSong(any()) } returns Single.error(TimeoutException())
        detailRepository = spyk(DetailRepository(searchTermDao))
    }

    private fun createMockedList() {
        val songA = getSampleSong("A")
        val songB = getSampleSong("B")
        mockedSongList = listOf(songA, songB)
    }

    private fun getSampleSong(someString: String) =
        Song(
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString
        )

}