package com.example.itunesdemo.list.viewmodel

import com.example.itunesdemo.core.data.network.Api
import com.example.itunesdemo.core.data.network.model.SearchResponse
import com.example.itunesdemo.core.data.network.model.Song
import com.example.itunesdemo.list.data.repository.SongListRepository
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeoutException

class SongListViewModelTests {

    @MockK
    lateinit var api: Api

    @MockK
    lateinit var mockedResult: SearchResponse

    @MockK
    lateinit var songListRepository: SongListRepository

    private lateinit var viewModel: SongListViewModel

    private lateinit var mockedSongList: List<Song>

    private val someTerm = "some term"

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        createMockedList()
    }

    @Test
    fun testSuccessSearch() {
        setupSuccessApi()
        viewModel.searchTerm = someTerm
        testCallbacks()
    }

    @Test
    fun testBadSearch() {
        setupBadApi()
        viewModel.searchTerm = someTerm
        testCallbacks()
    }

    private fun testCallbacks() {
        val callback = slot<(List<Song>) -> Unit>()
        val errorCallback = slot<(String) -> Unit>()

        verify {
            songListRepository.searchTerm(
                someTerm,
                capture(callback),
                capture(errorCallback)
            )
        }
        assertTrue(callback.isCaptured)
        assertTrue(errorCallback.isCaptured)
        assertNotNull(callback.captured)
        assertNotNull(errorCallback.captured)
    }

    private fun setupSuccessApi() {
        every { mockedResult.results } returns mockedSongList
        every { api.searchSong(any()) } returns Single.just(mockedResult)
        viewModel = spyk(SongListViewModel(songListRepository))
    }

    private fun setupBadApi() {
        every { mockedResult.results } returns mockedSongList
        every { api.searchSong(any()) } returns Single.error(TimeoutException())
        viewModel = spyk(SongListViewModel(songListRepository))
    }

    private fun createMockedList() {
        val songA = getSampleSong("A")
        val songB = getSampleSong("B")
        mockedSongList = listOf(songA, songB)
    }

    private fun getSampleSong(someString: String) =
        Song(
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString
        )

}