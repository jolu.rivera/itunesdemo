package com.example.itunesdemo.list.data.dao

import com.example.itunesdemo.core.data.network.Api
import com.example.itunesdemo.core.data.network.model.SearchResponse
import com.example.itunesdemo.core.data.network.model.Song
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeoutException

class SearchTermDaoTests {

    @MockK
    lateinit var api: Api

    @MockK
    lateinit var mockedResult: SearchResponse

    lateinit var mockedSongList: List<Song>

    lateinit var dao: SearchTermDao

    private val someTerm = "some term"

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setErrorHandler {}
        createMockedList()
    }

    @Test
    fun testSuccessExecution() {
        setupSuccessApi()
        testCallbacks()
    }

    @Test
    fun testBadExecution() {
        setupBadApi()
        testCallbacks()
    }

    private fun testCallbacks() {
        val mockedCallback: (List<Song>) -> Unit = mockk()
        val callback = slot<(List<Song>) -> Unit>()

        val mockedErrorCallback: (String) -> Unit = mockk()
        val errorCallback = slot<(String) -> Unit>()

        dao.searchTerm(someTerm, mockedCallback, mockedErrorCallback)
        verify { dao.searchTerm(someTerm, capture(callback), capture(errorCallback)) }
        assertTrue(callback.isCaptured)
        assertTrue(errorCallback.isCaptured)
        assertNotNull(callback.captured)
        assertNotNull(errorCallback.captured)
    }


    private fun setupSuccessApi() {
        every { mockedResult.results } returns mockedSongList
        every { api.searchSong(any()) } returns Single.just(mockedResult)
        dao = spyk(SearchTermDao(api))
    }

    private fun setupBadApi() {
        every { mockedResult.results } returns mockedSongList
        every { api.searchSong(any()) } returns Single.error(TimeoutException())
        dao = spyk(SearchTermDao(api))
    }

    private fun createMockedList() {
        val songA = getSampleSong("A")
        val songB = getSampleSong("B")
        mockedSongList = listOf(songA, songB)
    }

    private fun getSampleSong(someString: String) =
        Song(
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString,
            someString
        )

}