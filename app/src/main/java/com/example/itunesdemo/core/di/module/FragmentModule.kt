package com.example.itunesdemo.core.di.module

import com.example.itunesdemo.detail.view.DetailFragment
import com.example.itunesdemo.list.view.SongListFragment
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = [AndroidInjectionModule::class])
abstract class FragmentModule {

    /*
     * Song List Fragment
     */
    @ContributesAndroidInjector
    abstract fun bindSongListFragment(): SongListFragment

    /*
     * Detail Fragment
     */
    @ContributesAndroidInjector
    abstract fun bindDetailFragment(): DetailFragment


}