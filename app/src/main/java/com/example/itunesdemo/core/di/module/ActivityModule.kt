package com.example.itunesdemo.core.di.module

import com.example.itunesdemo.core.view.CoreActivity
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = [AndroidInjectionModule::class])
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun bindCoreActivity(): CoreActivity

}