package com.example.itunesdemo.core.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.itunesdemo.core.view.definition.BaseFragmentView
import com.example.itunesdemo.core.viewmodel.BaseViewModel
import com.example.itunesdemo.core.viewmodel.state.BaseViewState
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 *
 * @param VIEW_MODEL refers to the ViewModel class of the feature.
 *
 * @param DATA_BINDING refers to the auto-generated DataBinding class for your fragment.
 *
 * @param FRAGMENT_VIEW refers to the interface implemented by your inner fragment,
 * it must be implemented with data-binding purposes. (It extends from [BaseFragmentView])
 *
 */
abstract class BaseFragment<VIEW_MODEL : BaseViewModel<*>,
        DATA_BINDING : ViewDataBinding,
        FRAGMENT_VIEW : BaseFragmentView> :
    Fragment() {

    protected lateinit var mViewModel: VIEW_MODEL
    private lateinit var mViewDataBinding: DATA_BINDING
    private lateinit var mBackCallback: OnBackPressedCallback

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    abstract fun getViewModelClass(): Class<VIEW_MODEL>

    /**
     * @return the interface implemented by this fragment with
     * data-binding purposes.
     */
    abstract fun getFragmentViewClass(): Class<FRAGMENT_VIEW>

    /**
     * @return the layout resource id of your fragment's view.
     */
    @LayoutRes
    abstract fun getLayoutRes(): Int

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUpObservers()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProvider(this, viewModelFactory)[getViewModelClass()]
        if (handleBackCallback()) initializeBackPressedCallback()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mViewDataBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
        mViewDataBinding.apply {
            lifecycleOwner = viewLifecycleOwner
            setVariable(BR.viewModel, mViewModel)
            setVariable(BR.viewInteraction,
                getFragmentView(getFragmentViewClass())
            )
            this@BaseFragment.setupBeforeBind(this)
            executePendingBindings()
        }

        return mViewDataBinding.root
    }

    /**
     * Override this method to perform operations before executing all
     * pending bindings.
     */
    protected open fun setupBeforeBind(mViewDataBinding: DATA_BINDING) {
        // Override to do whatever you need here
    }

    /**
     * Override this method to allow your fragment to handle back
     * event at [onBackPressed].
     */
    protected open fun handleBackCallback(): Boolean = false

    /**
     * Override this method to handle back event only if you
     * got true at [handleBackCallback].
     */
    protected open fun onBackPressed() {
        // Override to handle back event
    }

    /**
     * Override this method to implement the handling for ViewStates
     */
    protected open fun renderViewState(state: BaseViewState) {
        // Override this method if is required
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mViewModel.clearState()
    }

    private fun getFragmentView(viewClass: Class<FRAGMENT_VIEW>): FRAGMENT_VIEW {
        return viewClass.cast(this)!!
    }

    private fun initializeBackPressedCallback() {
        mBackCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, mBackCallback)
    }

    private fun setUpObservers() {
        mViewModel.let { viewModel ->
            viewModel.viewState.observe(viewLifecycleOwner, Observer { renderViewState(it) })
        }
    }

}

