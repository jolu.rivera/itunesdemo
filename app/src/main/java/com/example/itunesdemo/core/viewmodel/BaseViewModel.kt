package com.example.itunesdemo.core.viewmodel

import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.itunesdemo.core.data.repository.BaseRepository
import com.example.itunesdemo.core.viewmodel.state.BaseViewState

open class BaseViewModel<REPOSITORY: BaseRepository> (protected val repository: REPOSITORY? = null) : ViewModel(), Observable {

    val viewState: LiveData<BaseViewState>
        get() = innerViewState

    private val innerViewState: MutableLiveData<BaseViewState> = MutableLiveData()

    private val callbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.remove(callback)
    }

    fun notifyChange() {
        callbacks.notifyCallbacks(this, 0, null)
    }

    fun notifyPropertyChanged(fieldId: Int) {
        callbacks.notifyCallbacks(this, fieldId, null)
    }

    fun clearState() {
        updateState(BaseViewState.ClearState)
    }

    fun updateState(state: BaseViewState) {
        innerViewState.value = state
    }


}