package com.example.itunesdemo.core.viewmodel.state

open class BaseViewState {

    var errorMessage: String = ""
    object ClearState : BaseViewState()
}