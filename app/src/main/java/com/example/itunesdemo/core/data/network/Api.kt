package com.example.itunesdemo.core.data.network

import com.example.itunesdemo.core.constants.ParamNames
import com.example.itunesdemo.core.constants.ServiceNames
import com.example.itunesdemo.core.data.network.model.SearchResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET(ServiceNames.SEARCH_TERM)
    fun searchSong(@Query(ParamNames.SEARCH_TERM) songName: String): Single<SearchResponse>

}