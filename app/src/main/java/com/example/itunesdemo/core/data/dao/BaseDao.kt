package com.example.itunesdemo.core.data.dao

import com.example.itunesdemo.core.data.network.Api
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

open class BaseDao @Inject constructor(protected val api: Api) {

    internal fun <T> executeServiceCall(
        service: Single<T>,
        onSuccess: (T) -> Unit,
        onError: (Throwable) -> Unit
    ): Disposable = service.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({ onSuccess.invoke(it) }, { onError.invoke(it) })

}