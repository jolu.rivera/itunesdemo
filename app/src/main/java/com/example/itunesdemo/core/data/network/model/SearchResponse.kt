package com.example.itunesdemo.core.data.network.model

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("results")
    val results: List<Song>
)