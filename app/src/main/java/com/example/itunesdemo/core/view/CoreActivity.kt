package com.example.itunesdemo.core.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.itunesdemo.R
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class CoreActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_core)
    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector
}
