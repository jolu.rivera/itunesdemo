package com.example.itunesdemo.core.data.network.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Song(
     @SerializedName("artistName")
     val artistName: String,

     @SerializedName("trackName")
     val trackName: String,

     @SerializedName("artworkUrl30")
     val artworkUrl30: String,

     @SerializedName("artworkUrl100")
     val artworkUrl100: String,

     @SerializedName("trackPrice")
     val trackPrice: String,

     @SerializedName("trackTimeMillis")
     val trackTimeMillis: String,

     @SerializedName("collectionName")
     val collectionName: String,

     @SerializedName("collectionViewUrl")
     val collectionViewUrl: String,

     @SerializedName("trackViewUrl")
     val trackViewUrl: String,

     @SerializedName("collectionPrice")
     val collectionPrice: String,

     @SerializedName("releaseDate")
     val releaseDate: String,

     @SerializedName("previewUrl")
     val previewUrl: String,

     @SerializedName("primaryGenreName")
     val primaryGenreName: String,

     @SerializedName("trackId")
     val trackId: String
) : Serializable