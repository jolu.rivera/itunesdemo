package com.example.itunesdemo.core.di.module

import androidx.lifecycle.ViewModel
import com.example.itunesdemo.core.di.annotation.ViewModelKey
import com.example.itunesdemo.detail.viewmodel.DetailViewModel
import com.example.itunesdemo.list.viewmodel.SongListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    /*
     * Song List
     */
    @Binds
    @IntoMap
    @ViewModelKey(SongListViewModel::class)
    abstract fun bindSongListViewModel(viewModel: SongListViewModel): ViewModel

    /*
     * Detail View Model
     */
    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    abstract fun bindDetailViewModel(viewModel: DetailViewModel): ViewModel

}