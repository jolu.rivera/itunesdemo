package com.example.itunesdemo.detail.viewmodel.state

import com.example.itunesdemo.core.data.network.model.Song
import com.example.itunesdemo.core.viewmodel.state.BaseViewState

sealed class DetailViewState : BaseViewState() {
    data class OnSearchSuccess(val albumTrackList: List<Song>) : DetailViewState()
    object OnSearchError : DetailViewState()
}