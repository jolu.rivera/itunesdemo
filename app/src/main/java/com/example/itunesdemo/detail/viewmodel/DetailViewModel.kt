package com.example.itunesdemo.detail.viewmodel

import androidx.databinding.Bindable
import com.example.itunesdemo.BR
import com.example.itunesdemo.core.data.network.model.Song
import com.example.itunesdemo.core.viewmodel.BaseViewModel
import com.example.itunesdemo.detail.data.repository.DetailRepository
import com.example.itunesdemo.detail.viewmodel.state.DetailViewState
import javax.inject.Inject

class DetailViewModel @Inject constructor(repository: DetailRepository) :
    BaseViewModel<DetailRepository>(repository) {

    var currentSong: Song? = null
        @Bindable get
        set(value) {
            field = value
            getAlbumTrackList()
            notifyPropertyChanged(BR.currentSong)
        }

    private fun getAlbumTrackList() {
        currentSong?.let { it ->
            repository?.searchByAlbum(
                it.collectionName,
                { updateState(DetailViewState.OnSearchSuccess(it)) },
                { updateState(DetailViewState.OnSearchError) })
        }
    }

}