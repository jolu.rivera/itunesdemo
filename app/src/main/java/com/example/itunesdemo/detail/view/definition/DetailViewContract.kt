package com.example.itunesdemo.detail.view.definition

import com.example.itunesdemo.core.view.definition.BaseFragmentView

interface DetailViewContract : BaseFragmentView {

    fun goBackToList()

}