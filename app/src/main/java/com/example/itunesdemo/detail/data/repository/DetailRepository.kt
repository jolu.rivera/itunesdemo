package com.example.itunesdemo.detail.data.repository

import com.example.itunesdemo.core.data.network.model.Song
import com.example.itunesdemo.core.data.repository.BaseRepository
import com.example.itunesdemo.list.data.dao.SearchTermDao
import javax.inject.Inject

class DetailRepository @Inject constructor(
    private val searchTermDao: SearchTermDao
) : BaseRepository() {

    fun searchByAlbum(
        albumName: String?,
        onSuccess: (List<Song>) -> Unit,
        onFailure: (String) -> Unit
    ) {
        searchTermDao.searchTerm(
            "$albumName",
            { handleSuccessResponse("$albumName", it, onSuccess) },
            { onFailure.invoke(it) })
    }

    private fun handleSuccessResponse(
        albumName: String,
        trackList: List<Song>,
        onSuccess: (List<Song>) -> Unit
    ) {
        val albumTrackList = arrayListOf<Song>()
        trackList.forEach {
            if (it.collectionName == albumName) albumTrackList.add(it)
        }
        onSuccess.invoke(albumTrackList)
    }

}