package com.example.itunesdemo.detail.view

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.itunesdemo.R
import com.example.itunesdemo.core.data.network.model.Song
import com.example.itunesdemo.core.view.BaseFragment
import com.example.itunesdemo.core.viewmodel.state.BaseViewState
import com.example.itunesdemo.databinding.FragmentDetailBinding
import com.example.itunesdemo.detail.view.definition.DetailViewContract
import com.example.itunesdemo.detail.viewmodel.DetailViewModel
import com.example.itunesdemo.detail.viewmodel.state.DetailViewState
import com.example.itunesdemo.list.view.SongAdapter
import com.example.itunesdemo.util.AudioPlayerUtil
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_detail.*

class DetailFragment : BaseFragment<DetailViewModel, FragmentDetailBinding, DetailViewContract>(),
    DetailViewContract {

    companion object {
        const val ARG_SONG = "ARG_SONG"
        val TAG = DetailFragment::class.java.name
    }

    private val songDialog by lazy { buildSongDialog() }
    private val progressRunnable by lazy { buildProgressRunnable(progressBar) }
    private val progressHandler by lazy { Handler() }
    private lateinit var progressBar: SeekBar

    override fun getViewModelClass(): Class<DetailViewModel> = DetailViewModel::class.java

    override fun getFragmentViewClass(): Class<DetailViewContract> = DetailViewContract::class.java

    override fun getLayoutRes(): Int = R.layout.fragment_detail

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val currentSong = arguments?.getSerializable(ARG_SONG)
        currentSong?.let {
            if (currentSong is Song) mViewModel.currentSong = currentSong
        }
    }

    override fun onStart() {
        super.onStart()
        mViewModel.currentSong?.let {
            Picasso.get().load(it.artworkUrl100).into(ivAlbumImage)
        }
    }

    override fun renderViewState(state: BaseViewState) {
        super.renderViewState(state)
        when (state) {
            is DetailViewState.OnSearchSuccess -> {
                Log.d(TAG, "SUCCESS: $state")
                rvAlbumSongs.setHasFixedSize(true)
                val viewManager = LinearLayoutManager(requireContext())
                val viewAdapter = SongAdapter(
                    { playSelectedSong(it) },
                    requireContext(),
                    state.albumTrackList
                )
                rvAlbumSongs.layoutManager = viewManager
                rvAlbumSongs.adapter = viewAdapter
            }
            is DetailViewState.OnSearchError -> showConnectionProblemsAlert()
        }
    }

    override fun goBackToList() {
        findNavController().popBackStack()
    }

    private fun playSelectedSong(song: Song) {
        AudioPlayerUtil.playAudio(
            requireContext(),
            song.previewUrl,
            { showSongDialog(song) },
            {
                showConnectionProblemsAlert()
                return@playAudio true
            },
            { stopPlaying() }
        )
    }


    private fun stopPlaying() {
        progressHandler.removeCallbacks(progressRunnable)
        songDialog.dismiss()
    }

    private fun showConnectionProblemsAlert() {
        AlertDialog.Builder(requireContext())
            .setMessage(getString(R.string.connection_issues))
            .setCancelable(false)
            .setPositiveButton(
                getString(R.string.ok_button)
            ) { _, _ -> goBackToList() }
            .show()
    }

    private fun showSongDialog(song: Song) {
        songDialog.setOnShowListener {
            val songImage = songDialog.findViewById<ImageView>(R.id.ivSongImage)
            val songName = songDialog.findViewById<TextView>(R.id.tvSongName)
            progressBar = songDialog.findViewById(R.id.sbProgress)
            songName.text = song.trackName
            Picasso.get().load(song.artworkUrl100).into(songImage)
            progressHandler.postDelayed(progressRunnable, 1000)
        }
        songDialog.show()
    }

    private fun buildSongDialog(): AlertDialog = AlertDialog.Builder(requireContext())
        .setView(R.layout.dialog_song_preview)
        .setCancelable(true)
        .setOnCancelListener {
            progressHandler.removeCallbacks(progressRunnable)
            AudioPlayerUtil.stopAudio()
        }
        .create()

    private fun buildProgressRunnable(progressBar: SeekBar) = object : Runnable {
        override fun run() {
            Log.d(TAG, "Progress bar: $progressBar")
            Log.d(TAG, "update progress: ${AudioPlayerUtil.getProgress()}")
            progressBar.progress = AudioPlayerUtil.getProgress()
            progressHandler.postDelayed(this, 1000)
        }

    }

}