package com.example.itunesdemo.list.data.dao

import com.example.itunesdemo.core.data.dao.BaseDao
import com.example.itunesdemo.core.data.network.Api
import com.example.itunesdemo.core.data.network.model.Song
import javax.inject.Inject

class SearchTermDao @Inject constructor(api: Api) : BaseDao(api) {

    fun searchTerm(
        searchTerm: String,
        onSuccess: (List<Song>) -> Unit,
        onError: (String) -> Unit
    ) {
        executeServiceCall(api.searchSong(searchTerm),
            {
                if (it.results.isNotEmpty()) onSuccess.invoke(it.results)
                else {
                    onError.invoke("Not found!")
                }
            },
            { onError.invoke("${it.message}") }
        )
    }

}