package com.example.itunesdemo.list.view

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.itunesdemo.R
import com.example.itunesdemo.core.data.network.model.Song
import com.example.itunesdemo.core.view.BaseFragment
import com.example.itunesdemo.core.viewmodel.state.BaseViewState
import com.example.itunesdemo.databinding.FragmentSongListBinding
import com.example.itunesdemo.detail.view.DetailFragment
import com.example.itunesdemo.list.view.definition.SongListViewContract
import com.example.itunesdemo.list.viewmodel.SongListViewModel
import com.example.itunesdemo.list.viewmodel.state.SongListViewState
import kotlinx.android.synthetic.main.fragment_song_list.*

class SongListFragment :
    BaseFragment<SongListViewModel, FragmentSongListBinding, SongListViewContract>(),
    SongListViewContract {

    companion object {
        val TAG: String = SongListFragment::class.java.simpleName
    }

    override fun getViewModelClass(): Class<SongListViewModel> = SongListViewModel::class.java

    override fun getFragmentViewClass(): Class<SongListViewContract> =
        SongListViewContract::class.java

    override fun getLayoutRes(): Int = R.layout.fragment_song_list

    override fun renderViewState(state: BaseViewState) {
        super.renderViewState(state)
        when (state) {
            is SongListViewState.OnSearchSuccess -> {
                Log.d(TAG, "SUCCESS: $state")
                tvNotFound.visibility = View.GONE
                rvSongList.setHasFixedSize(true)
                val viewManager = LinearLayoutManager(requireContext())
                val viewAdapter = SongAdapter(
                    { goToDetail(it) },
                    requireContext(),
                    state.songs
                )
                rvSongList.layoutManager = viewManager
                rvSongList.adapter = viewAdapter
            }
            is SongListViewState.OnSearchError -> {
                tvNotFound.visibility = View.VISIBLE
            }
        }
    }

    private fun goToDetail(song: Song) {
        val args = Bundle()
        args.putSerializable(DetailFragment.ARG_SONG, song)
        findNavController().navigate(R.id.action_songListFragment_to_detailFragment, args)
    }

}