package com.example.itunesdemo.list.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.itunesdemo.R
import com.example.itunesdemo.core.data.network.model.Song
import com.squareup.picasso.Picasso

class SongAdapter(
    private val songItemPicker: (Song) -> Unit,
    private val context: Context,
    private val songs: List<Song>
) :
    RecyclerView.Adapter<SongAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(
            R.layout.song_item_row,
            parent,
            false
        )
        return ViewHolder(view, songItemPicker)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val songName: String = songs[position].trackName
        val imageUrl: String = songs[position].artworkUrl100
        holder.songName.text = songName
        holder.song = songs[position]
        Picasso.get().load(imageUrl).into(holder.songImage)
    }

    override fun getItemCount(): Int {
        return songs.size
    }

    class ViewHolder internal constructor(itemView: View, songItemPicker: (Song) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        val songName: TextView = itemView.findViewById(R.id.tvSongName)
        val songImage: ImageView = itemView.findViewById(R.id.ivSongImage)

        var song: Song? = null

        init {
            itemView.setOnClickListener {
                song?.let { songItemPicker.invoke(it) }
            }
        }
    }

}