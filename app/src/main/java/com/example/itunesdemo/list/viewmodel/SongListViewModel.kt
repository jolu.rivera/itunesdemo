package com.example.itunesdemo.list.viewmodel

import androidx.databinding.Bindable
import com.example.itunesdemo.BR
import com.example.itunesdemo.core.viewmodel.BaseViewModel
import com.example.itunesdemo.list.data.repository.SongListRepository
import com.example.itunesdemo.list.viewmodel.state.SongListViewState
import javax.inject.Inject

class SongListViewModel @Inject constructor(repository: SongListRepository) :
    BaseViewModel<SongListRepository>(repository) {

    var searchTerm: String = ""
        @Bindable get
        set(value) {
            field = value
            searchTerm(field)
            notifyPropertyChanged(BR.searchTerm)
        }

    private fun searchTerm(term: String) {
        repository?.searchTerm(
            term,
            { updateState(SongListViewState.OnSearchSuccess(it)) },
            { updateState(SongListViewState.OnSearchError) })
    }

}