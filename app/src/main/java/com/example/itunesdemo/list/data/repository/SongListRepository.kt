package com.example.itunesdemo.list.data.repository

import com.example.itunesdemo.core.data.network.model.Song
import com.example.itunesdemo.core.data.repository.BaseRepository
import com.example.itunesdemo.list.data.dao.SearchTermDao
import javax.inject.Inject

class SongListRepository @Inject constructor(
    private val searchTermDao: SearchTermDao
) : BaseRepository() {

    fun searchTerm(
        searchTerm: String,
        onSuccess: (List<Song>) -> Unit,
        onFailure: (String) -> Unit
    ) {
        searchTermDao.searchTerm(
            searchTerm,
            { onSuccess.invoke(it) },
            { onFailure.invoke(it) })
    }

}