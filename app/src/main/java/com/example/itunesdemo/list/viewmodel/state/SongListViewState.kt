package com.example.itunesdemo.list.viewmodel.state

import com.example.itunesdemo.core.data.network.model.Song
import com.example.itunesdemo.core.viewmodel.state.BaseViewState

sealed class SongListViewState : BaseViewState() {
    data class OnSearchSuccess(val songs: List<Song>) : SongListViewState()
    object OnSearchError : SongListViewState()
}