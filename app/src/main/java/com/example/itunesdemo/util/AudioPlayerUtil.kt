package com.example.itunesdemo.util

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log
import kotlin.math.roundToInt


object AudioPlayerUtil {

    private val TAG = AudioPlayerUtil::class.java.name
    private var mediaPlayer: MediaPlayer? = null

    fun playAudio(
        context: Context?,
        url: String?,
        onPrepared: () -> Unit,
        onError: () -> Boolean,
        onComplete: () -> Unit
    ) {
        try {
            mediaPlayer = MediaPlayer.create(context, Uri.parse(url))
            mediaPlayer?.setOnCompletionListener { killMediaPlayer() }
            mediaPlayer?.setOnPreparedListener { onPrepared.invoke() }
            mediaPlayer?.setOnErrorListener { _, _, _ -> onError.invoke() }
            mediaPlayer?.setOnCompletionListener { onComplete.invoke() }
            mediaPlayer?.start()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getProgress(): Int = mediaPlayer?.let {
        Log.d(TAG, "Duration: ${it.duration} - Current: ${it.currentPosition}")
        val factor: Double = 100 / it.duration.toDouble()
        val percentPosition = factor * it.currentPosition
        Log.d(TAG, "Percent position: $percentPosition")
        return@let percentPosition.roundToInt()
    } ?: 0

    fun stopAudio() {
        mediaPlayer?.let {
            it.stop()
            killMediaPlayer()
        }
    }

    private fun killMediaPlayer() {
        mediaPlayer?.let {
            try {
                it.reset()
                it.release()
                mediaPlayer = null
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

}